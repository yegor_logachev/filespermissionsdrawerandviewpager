package com.logacfg.filesandpermissions;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Yegor on 8/19/17.
 */

public class FileAdapter extends RecyclerView.Adapter<FileAdapter.FileViewHolder> {

    private ArrayList<String> files;
    private LayoutInflater inflater;
    @Nullable
    private OnItemClickListener listener;

    public FileAdapter(Context context, OnItemClickListener listener) {
        inflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    public void setFiles(ArrayList<String> files) {
        this.files = files;
        notifyDataSetChanged();
    }

    @Override
    public FileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingBottom(), 200);
        return new FileViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FileViewHolder holder, int position) {
        String filePath = files.get(position);
        holder.textView.setText(filePath);
    }

    @Override
    public int getItemCount() {
        return files == null ? 0 : files.size();
    }

    class FileViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textView;

        FileViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            textView = (TextView) itemView;
        }

        @Override
        public void onClick(View view) {
            if (listener != null) {
                listener.onItemClick(getAdapterPosition());
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}
