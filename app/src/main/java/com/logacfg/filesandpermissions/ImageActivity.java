package com.logacfg.filesandpermissions;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

/**
 * Created by Yegor on 8/19/17.
 */

public class ImageActivity extends AppCompatActivity {

    private static final String FILE_PATH_KEY = "file_path_key";

    ImageView imageView;

    public static Intent start(Context context, String path) {
        Intent starter = new Intent(context, ImageActivity.class);
        starter.putExtra(FILE_PATH_KEY, path);
        return starter;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_image);
        imageView = (ImageView) findViewById(R.id.imageView);
        Intent intent = getIntent();
        String filePath = intent.getStringExtra(FILE_PATH_KEY);
        imageView.setImageBitmap(decodeImage(filePath));
    }

    private Bitmap decodeImage(String filePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        return BitmapFactory.decodeFile(filePath, options);
    }
}
