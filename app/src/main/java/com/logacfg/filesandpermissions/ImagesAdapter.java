package com.logacfg.filesandpermissions;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by Yegor on 8/19/17.
 */

public class ImagesAdapter extends PagerAdapter {

    private ArrayList<String> files;
    private LayoutInflater inflater;

    public ImagesAdapter(Context context, ArrayList<String> files) {
        inflater = LayoutInflater.from(context);
        this.files = files;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView view = (ImageView) inflater.inflate(R.layout.layout_image, container, false);
        container.addView(view);
        String filePath = files.get(position);
        Bitmap bitmap = decodeImage(filePath);
        view.setImageBitmap(bitmap);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return files == null ? 0 : files.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    private Bitmap decodeImage(String filePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        return BitmapFactory.decodeFile(filePath, options);
    }
}
