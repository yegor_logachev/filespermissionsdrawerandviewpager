package com.logacfg.filesandpermissions;

import android.Manifest;
import android.app.Application;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements FileAdapter.OnItemClickListener {

    private final static int READ_FILES_PERMISSION_REQUEST_CODE = 10;

    private FileAdapter adapter;
    private ImageView imageView;
    private DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Application application = getApplication();



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        supportActionBar.setDisplayHomeAsUpEnabled(true);
        supportActionBar.setHomeButtonEnabled(true);


        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new FileAdapter(this, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,
                drawerLayout, toolbar, R.string.open_drawer, R.string.close_drawer) {
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        viewPager = (ViewPager) findViewById(R.id.viewPager);

//        imageView = (ImageView) findViewById(R.id.imageView);
        tryLoadFiles();
    }

    public MyApplication getMyApplication() {
        return (MyApplication) getApplication();
    }

    private void tryLoadFiles() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            ArrayList<String> strings = loadFiles(Environment.getExternalStorageDirectory());
            adapter.setFiles(strings);
            ImagesFragmentAdapter imagesAdapter = new ImagesFragmentAdapter(getSupportFragmentManager(), strings);
            viewPager.setAdapter(imagesAdapter);
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE },
                    READ_FILES_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == READ_FILES_PERMISSION_REQUEST_CODE && grantResults.length > 0 &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            tryLoadFiles();
        } else {
            Toast.makeText(this, "Please provide permissions, I wanna post your naked photos in social", Toast.LENGTH_SHORT).show();
        }
    }

    private ArrayList<String> loadFiles(File file) {
        ArrayList<String> files = new ArrayList<>();
        if (file != null && file.exists()) {
            if (file.isFile() && isImage(file)) {
                files.add(file.getAbsolutePath());
            } else if (file.isDirectory()) {
                File[] filesArr = file.listFiles();
                if (filesArr != null) {
                    for (File file1 : filesArr) {
                        ArrayList<String> strings = loadFiles(file1);
                        files.addAll(strings);
                    }
                }
            }
        }
        return files;
    }

    private boolean isImage(File file) {
        return file.getPath().endsWith(".jpg") || file.getPath().endsWith(".jpeg")
                || file.getPath().endsWith(".png");
    }

    @Override
    public void onItemClick(int position) {
        viewPager.setCurrentItem(position);
        drawerLayout.closeDrawer(Gravity.START);
    }
}
