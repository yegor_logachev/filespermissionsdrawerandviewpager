package com.logacfg.filesandpermissions;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by Yegor on 8/19/17.
 */

public class ImagesFragmentAdapter extends FragmentPagerAdapter {

    private ArrayList<String> files;

    public ImagesFragmentAdapter(FragmentManager fm, ArrayList<String> files) {
        super(fm);
        this.files = files;
    }

    @Override
    public Fragment getItem(int position) {
        String filePath = files.get(position);
        return ImageFragment.newInstance(filePath);
    }

    @Override
    public int getCount() {
        return files == null ? 0 : files.size();
    }
}
