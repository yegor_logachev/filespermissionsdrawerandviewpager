package com.logacfg.filesandpermissions;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by Yegor on 8/19/17.
 */

public class ImageFragment extends Fragment {

    private ImageView imageView;

    private static final String FILE_PATH_BUNDLE_KEY = "file_path_bundle_key";

    public static ImageFragment newInstance(String imagePath) {
        Bundle args = new Bundle();
        args.putString(FILE_PATH_BUNDLE_KEY, imagePath);
        ImageFragment fragment = new ImageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        imageView = (ImageView) inflater.inflate(R.layout.layout_image, container, false);
        String filePath = getArguments().getString(FILE_PATH_BUNDLE_KEY);
        Bitmap bitmap = decodeImage(filePath);
        imageView.setImageBitmap(bitmap);
        return imageView;
    }

    private Bitmap decodeImage(String filePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        return BitmapFactory.decodeFile(filePath, options);
    }
}
